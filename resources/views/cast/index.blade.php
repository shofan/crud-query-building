@extends('master')

@section('title')
List Kategori
@endsection

@section('isi')


<a href="/cast/create" class="btn btn-primary mb-3">Tambah</a>
<table class="table">
    <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur</th>
            <th scope="col">Bio</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$value)
        <tr>
            <td>{{$key + 1}}</th>
            <td>{{$value->nama}}</td>
            <td>{{$value->umur}}</td>
            <td>{{$value->bio}}</td>
            <td>
                
                <form action="/cast/{{$value->id}}" method="post">
                <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/cast/{{$value->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                    @csrf 
                    @method('DELETE')
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </td>
         </tr>
        @empty
        <tr>
            <td colspan="4" align="center">No data</td>
        </tr>  
        @endforelse              
        </tbody>
</table>

@endsection