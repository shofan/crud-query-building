<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
        <h3>Sign Up Form</h3>
        <form action="/welcome" method="post">
            @csrf
            <label>First Name:</label> <br> <br>
            <input type="text" name="nama"> <br><br>
            <label>Last Name:</label> <br> <br>
            <input type="text" name="namaakhir"> <br> <br>
            <label>Gender:</label> <br> <br>
            <input type="radio" name="gender" value="male"> Male <br>
            <input type="radio" name="gender" value="fimale"> Female <br>
            <input type="radio" name="gender" value="other"> Other <br> <br>
            <label>Nationality:</label> <br> <br>
            <select name="nat"> <br>
                <option value="1">Indonesian</option>
                <option value="2">American</option>
                <option value="3">English</option> <br> <br>
            <select> <br> <br>
            <label>Language Spoken:</label> <br> <br>
            <input type="checkbox" name="ls" value="indo"> Indonesian <br>
            <input type="checkbox" name="ls" value="ame"> American <br>
            <input type="checkbox" name="ls" value="eng"> English <br> <br>
            <label>Bio:</label> <br> <br>
            <textarea name="Bio" cols="30" rows="10"></textarea> <br>
            <input type="submit" value="Sign Up"> 
        </form>
</body>
</html>